import SlimSelect from "slim-select";

export default () => {
  const slimSelects = document.querySelectorAll(".slim-select");
  if (slimSelects) {
    Array.from(slimSelects).forEach(select => {
      new SlimSelect({
        select,
        showSearch: false
      });
    });
  }
};
