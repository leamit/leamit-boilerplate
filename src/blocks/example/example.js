import flatpickr from "flatpickr";
import Flickity from "flickity";
import TweenMax from "gsap/TweenMax";
import Scrollmagic from "scrollmagic";
import addIndicators from "scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min";
import gsapAnimation from "scrollmagic/scrollmagic/minified/plugins/animation.gsap.min";
import lightGallery from "lightgallery.js";
import lightGalleryThunmnail from "lg-thumbnail.js";
import lightGalleryFullScreen from "lg-fullscreen.js";
import lightGalleryAutoplay from "lg-autoplay.js";
import MicroModal from "micromodal/dist/micromodal.es";
import simplebar from "simplebar";

export default () => {
  const datePickerInput = document.querySelector("#date-picker");
  const sliderNode = document.querySelector(".example__slider");
  if (datePickerInput) {
    flatpickr(datePickerInput, {});
  }

  if (sliderNode) {
    const slider = new Flickity(sliderNode, {});
  }

  // scrollmagic + gsap example

  if (document.querySelector(".example__animation")) {
    const controller = new Scrollmagic.Controller();

    new Scrollmagic.Scene({
      triggerElement: ".example__animation",
      triggerHook: 0.75,
      offset: 150,
      duration: 300,
      reverse: true
    })
      .setPin(".example__animation-wrap")
      .setTween(".example__animation-item", 1, { rotation: 360 })
      .addIndicators()
      .addTo(controller);
  }

  // gallery example
  if (document.querySelector("#gallery")) {
    window.lightGallery(document.getElementById("gallery"), {
      thumbnail: true,
      mode: "lg-fade"
    });
  }

  // init micromodal
  MicroModal.init();
};
