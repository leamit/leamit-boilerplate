// Load plugins
import gulp from "gulp";
import browserSync from "browser-sync";
import pug from "gulp-pug";
import prettyHtml from "gulp-pretty-html";
import concat from "gulp-concat";
import sourcemaps from "gulp-sourcemaps";
import plumber from "gulp-plumber";
import rename from "gulp-rename";
import imagemin from "gulp-imagemin";
import del from "del";
import babel from "rollup-plugin-babel";
import resolve from "rollup-plugin-node-resolve";
import commonjs from "rollup-plugin-commonjs";
import { terser } from "rollup-plugin-terser";
import svgSprite from "gulp-svg-sprites";
// import svgstore from "gulp-svgstore";
import postcss from "gulp-postcss";
import postcssScss from "postcss-scss";

const os = require("os");

os.tmpDir = os.tmpdir;

const rollup = require("rollup");

const server = browserSync.create();

// plugins postcss
const plugins = [
  require("postcss-nested"),
  require("postcss-import"),
  require("postcss-extend"),
  require("autoprefixer"),
  require("doiuse")({
    browsers: [">2%", "not op_mini all", "not ie <=11", "not and_uc <=11.8"],
    ignore: [] // an optional array of features to ignore
    // ignoreFiles: ["**/_import.pcss"], // an optional array of file globs to match against original source file path, to ignore
    // onFeatureUsage(usageInfo) {
    //   console.log(usageInfo.message);
    // }
  }),
  require("postcss-preset-env")({
    stage: 4
  }),
  require("cssnano")
];

// html tasks
gulp.task("html", done => {
  gulp
    .src("src/pages/*.pug")
    .pipe(plumber())
    .pipe(pug({}))
    .pipe(gulp.dest("dist"))
    .pipe(
      server.stream({
        once: true
      })
    );
  done();
});

gulp.task("pretty-html", done => {
  gulp
    .src("dist/**/*.html")
    .pipe(
      prettyHtml({
        indent_size: 2
      })
    )
    .pipe(gulp.dest("dist"));
  done();
});

// Styles
gulp.task("styles", done => {
  gulp
    .src(["src/styles/main/*.pcss", "src/blocks/**/*.pcss"])
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(concat("styles.pcss"))
    .pipe(postcss(plugins, { parser: postcssScss, syntax: postcssScss }))
    .pipe(rename({ suffix: ".min", extname: ".css" }))
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest("dist/css/"))
    .pipe(server.stream());
  done();
});

gulp.task("libs-css", done => {
  gulp
    .src(["src/styles/vendor/libs.pcss"])
    .pipe(plumber())
    // eslint-disable-next-line global-require
    .pipe(
      postcss([
        require("postcss-nested"),
        require("postcss-import"),
        require("cssnano")
      ])
    )
    .pipe(rename({ suffix: ".min", extname: ".css" }))
    .pipe(gulp.dest("dist/css/"))
    .pipe(server.stream());
  done();
});

// Scripts
gulp.task("js", () =>
  rollup
    .rollup({
      input: "./src/js/main.js",
      plugins: [
        resolve({
          mainFields: ["jsnext", "main"],
          browser: true
        }),
        commonjs(),
        babel()
      ]
    })
    .then(bundle =>
      bundle.write({
        file: "./dist/js/main.js",
        format: "iife",
        name: "main",
        sourcemap: true
      })
    )
);

gulp.task("js-prod", () =>
  rollup
    .rollup({
      input: "./src/js/main.js",
      plugins: [
        resolve({
          mainFields: ["jsnext", "main"],
          browser: true
        }),
        commonjs(),
        babel(),
        terser()
      ]
    })
    .then(bundle =>
      bundle.write({
        file: "./dist/js/main.js",
        format: "iife",
        name: "main",
        sourcemap: true
      })
    )
);

// Images
gulp.task("images-copy", done => {
  gulp.src("src/images/**").pipe(gulp.dest("dist/images"));
  done();
});

gulp.task("images-prod", done => {
  gulp
    .src("src/images/**")
    .pipe(
      imagemin({
        optimizationLevel: 3,
        progressive: true,
        interlaced: true,
        verbose: true
      })
    )
    .pipe(gulp.dest("dist/images"));
  done();
});

gulp.task("svg-sprite", done => {
  gulp
    .src("src/images/svg-sprite/*.svg")
    // .pipe(replace("&gt;", ">"))
    .pipe(
      svgSprite({
        mode: "symbols",
        preview: false,
        selector: "icon-%f",
        svg: {
          symbols: "sprite.svg"
        }
      })
    )
    .pipe(gulp.dest("dist/images/svg-sprite"));
  done();
});

gulp.task("clean", done => {
  del("dist/*");
  done();
});

gulp.task("copy", done => {
  gulp.src("src/fonts/**/*").pipe(gulp.dest("dist/fonts"));
  gulp.src("src/resources/**/*").pipe(gulp.dest("dist/resources"));
  done();
});

// server
gulp.task("dev", () => {
  server.init({
    server: {
      baseDir: "dist",
      index: "index.html"
    },
    notify: true, // false
    port: 3000,
    open: false
    // tunnel: true,
    // tunnel: "projectnametest" //Demonstration page: http://projectname.localtunnel.me
  });

  gulp
    .watch("src/**/*.{png, jpg, svg}")
    .on("change", gulp.series("images-copy", "svg-sprite", "refresh"));
  gulp.watch("src/**/*.js").on("change", gulp.series("js", "refresh"));
  gulp.watch("src/**/*.pcss", gulp.series("styles"));
  gulp.watch("src/**/*.pug").on("change", gulp.series("html"));
});

gulp.task("refresh", done => {
  server.reload();
  done();
});

// build
gulp.task("build", done =>
  gulp.series(
    "clean",
    "copy",
    "html",
    "pretty-html",
    "styles",
    "libs-css",
    "js-prod",
    "images-prod",
    "svg-sprite"
  )(done)
);

// default
gulp.task("default", done => {
  gulp.series("clean", "copy");
  gulp.parallel(
    gulp.series(
      "html",
      "pretty-html",
      "styles",
      "libs-css",
      "images-prod"
      // "svg-sprite"
    ),
    "js-prod"
  )(done);
});
